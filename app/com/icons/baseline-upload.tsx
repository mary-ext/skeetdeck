import { createIcon } from './_icon';

const UploadIcon = createIcon([['M5 20h14v-2H5zm0-10h4v6h6v-6h4l-7-7z']]);

export default UploadIcon;
