import { createIcon } from './_icon';

const DownloadIcon = createIcon([['M5 20h14v-2H5zM19 9h-4V3H9v6H5l7 7z']]);

export default DownloadIcon;
